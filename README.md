# README

Example project of a dockerized node and mongodb service. Starting point
for a dockerized node project, like an API.

## Usage

Start dockerized db and node dev environment:
```bash
yarn dev
```

Start docker container in production:
```bash
yarn prod
```