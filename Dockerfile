FROM node:7

MAINTAINER Simon Breiter hello@simonbreiter.com

RUN mkdir -p /usr/src/node-docker
WORKDIR /usr/src/node-docker
ADD . /usr/src/node-docker

RUN npm install && \
    npm install -g nodemon
